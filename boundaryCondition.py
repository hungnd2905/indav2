import pickle
from pathlib import Path
from storage import storeResults

results = []
frozen_new_ID = []

root = Path(".")


def loadVariables():
    global frozen_new_ID
    global results

    # load results
    inputFile = 'results.data'
    inputPath = root / "Data" / inputFile
    fd = open(inputPath, 'rb')
    results = pickle.load(fd)

    #load frozenNewID
    inputFile = 'frozenNewID.data'
    inputPath = root / "Data" / inputFile
    fd = open(inputPath, 'rb')
    frozen_new_ID= pickle.load(fd)

def boundaryCondition(input):
    # Ensure that frozen cells stay frozen
    for t in range(len(frozen_new_ID)):
        #get the new frozen id of each image
        temp_frozen_ID = frozen_new_ID[t]
        if temp_frozen_ID:
            #for each new frozen ID:
            for h in range(len(temp_frozen_ID)):
                #for all images from index (t +1) to the end,...
                for i in range(t+1,len(input)):
                    #go through all 96 position of PCR Tray
                    for j in range(len(input[i])):
                        for k in range(len(input[i][j])):
                            #check if ID of each cell == new frozen ID?
                            if input[i][j][k][8] == temp_frozen_ID[h] and input[i][j][k][4] == 0:
                                #change class from 0 to 1
                                results_list= list(input[i][j][k])
                                results_list[4] =1
                                input[i][j][k] =tuple(results_list)
    storeResults(input)



# Defining main function to be run
def main():
    loadVariables()
    boundaryCondition(results)


# Checking if current namespace is main, that is file is not imported
if __name__ == '__main__':
    # Implementing main() function
    main()
