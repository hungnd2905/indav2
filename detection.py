import csv
import os
import pickle
import shutil
import time

from matplotlib import pyplot

from yolo3 import yolo3
import cv2
from pathlib import Path
import pandas as pd
import sys
import matplotlib.pyplot as plt
import numpy as np
import sqlite3
import time

from storage import storeFixPos
from storage import storeFrozenNewID
from storage import storeImagePath
from storage import storeResults
from storage import storeResultsOriginal
from storage import storeImageFilename

import collections
import oldProgram
import drawBoundingBox
import boundaryCondition

#initialize global variables

# folder path to be detected
folderPath = ""
# destination directory to save result
des_dir = ""
# list of all image file name in selected folder
image_filename = []
# list of all image path in selected folder
imagePath = []
# list of current result of detected image
result = []
# list of all results of detected image
results = []

# Data to export as CSV
temperature = []
frozen_total = []
f_ice = []
frozen_new_nr = []
frozen_new_ID = []

root = Path(".")
def loadVariables():
    global folderPath
    inputFile = 'folderPath.data'
    inputPath = root / "Data" / inputFile
    fd = open(inputPath, 'rb')
    folderPath = pickle.load(fd)

# destination directory to save result
def desDir(input):
    global folderPath
    global des_dir
    # get the current working dir
    src_dir = os.getcwd()
    # destination directory to save result
    des_dir = src_dir + input + os.path.basename(folderPath)

    # create destination directory
    if os.path.exists(des_dir):
        shutil.rmtree(des_dir)
        time.sleep(0.1)
    os.makedirs(des_dir)

    print("Path of destination folder:")
    print(des_dir)
    print("--------")


def runDetection():
    global folderPath
    global image_filename
    global imagePath
    global result
    global results

    ######### read jpg files in selected folder
    for r, d, f in os.walk(folderPath):
        for file in f:
            if file.endswith(".jpg"):
                fname = tuple([os.path.join(r, file)] + ['*.jpg'])
                print("------------------------------------------------------------------")
                print("Detected Image:")
                print(fname[0])
                #print(os.path.basename(fname[0]))

                # checking if image to be detected really belongs to selected folder?
                # avoiding the case wrong file with ending ".jpg" in selected folder
                split_fname = os.path.basename(fname[0]).split("_")
                # this method check if a substring is in a string
                if split_fname[1] in os.path.basename(folderPath):
                    #save image file name
                    image_filename.append(os.path.basename(fname[0]))

                    #save image path
                    imagePath.append(fname[0])

                    #run YOLO3 Detection
                    result = loadImage(fname)

                    #save detected result
                    results.append(result)

    #Store list of imagePath
    storeImagePath(imagePath)
    #Store list of image_filename
    storeImageFilename(image_filename)
    #Store fix Position (last result)
    storeFixPos(result)
    #Store detected results
    storeResults(results)
    #Store detected results copy
    storeResultsOriginal(results)

def loadImage(input):
    global results
    global image_filename
    image = cv2.imread(input[0], cv2.IMREAD_COLOR)
    output = yolo3(input[0])

    # if Picture ist not detected correctly, skip and append last result
    if len(output) < 12:
        #TODO Test first image error
        #if list of all results of detected image is empty, which means first image ist wrong detected
        #show error Text
        if not results:
            print("First image is captured not correctly, there are no result to be used for this first image ")
            print("Solution: copy the next correct captured image and exchange this to the first image")
        else:
            #use last result of detected image as current result
            output = results[-1]

            # add "Error_" at beginning of image_filename
            error_name = image_filename[-1]
            error_name = "Error_" + error_name
            image_filename[-1] = error_name

            #add "Error_" to imagePath
            error_path= imagePath[-1]
            error_path_dir = os.path.dirname(error_path)
            error_path_base = os.path.basename(error_path)
            error_path_base = "Error_" + error_path_base
            error_path_new = os.path.join(error_path_dir,error_path_base)
            imagePath[-1] = error_path_new

    return output

def getTemperature():
    global folderPath
    global temperature
    print("Path of Tlog file:")
    for r, d, f in os.walk(folderPath):
        for file in f:
            # read csv file
            if file.endswith("Tlog.csv"):
                fname_Tlog = os.path.join(r, file)
                print(fname_Tlog)
    print("-----------")

    df = pd.read_csv(fname_Tlog, sep=";", header=None, usecols=[3])
    temperature = df.values.tolist()
    temperature = [val for sublist in temperature for val in sublist]

def calculateVariables(input):
    global frozen_new_nr
    global frozen_total
    global f_ice
    global frozen_new_ID

    # old_pos: two dimensional array 8x12 to mark all current frozen cells in one image
    #init old_pos with all 0 value
    old_pos = [[0 for a in range(8)] for b in range(12)]
    #for each image in results
    for i in range(len(input)):
        #number of total frozen cells in each image
        temp_frozen = 0
        #list of new frozen cells in each image
        temp_pos_new = []
        #number of new frozen cells in each image
        frozen_new = 0
        #for each column in result
        for j in range(len(input[i])):
            #for each value in this column
            for k in range(len(input[i][j])):
                if input[i][j][k][4] == 1:
                    #find the first frozen ID
                    if old_pos[j][k] == 0:
                        old_pos[j][k] = 1
                        temp_pos = input[i][j][k][6]
                        temp_pos += input[i][j][k][7]
                        temp_pos_new.append(temp_pos)
                        frozen_new += 1

        for x in range(len(old_pos)):
            for y in range(len(old_pos[x])):
                if old_pos[x][y] == 1:
                    temp_frozen += 1

        #append all calculated variables from each image to list
        frozen_new_nr.append(frozen_new)
        frozen_total.append(temp_frozen)
        f_ice.append(temp_frozen / 96)
        frozen_new_ID.append(temp_pos_new)

        #store new frozen ID
        storeFrozenNewID(frozen_new_ID)

def plotChart(des):
    global temperature
    global f_ice
    global des_dir

    pyplot.xlim(round(min(temperature)) - 2.0, 0.0)
    pyplot.title('F_ice with YOLO')
    pyplot.ylabel('Fraction of frozen samples (f_ice)')
    pyplot.xlabel('Temperatur in C')
    fice = np.array(f_ice)
    pyplot.errorbar(temperature, fice, yerr=poisson_error(fice, 96), c='#E24A33', ecolor='salmon',
                    elinewidth=0.2)

    pyplot.savefig(os.path.join(des,'graph.png'), dpi=150, bbox_inches='tight')
    pyplot.close()

def exportCSV(filename,des):
    global image_filename
    global temperature
    global frozen_total
    global f_ice
    global frozen_new_nr
    global frozen_new_ID
    global des_dir
    fice_leer = []
    fice_ueer = []
    for i in range(len(f_ice)):
        l = poisson_error(f_ice[i], 96)[0]
        u = poisson_error(f_ice[i], 96)[1]
        fice_leer.append(l)
        fice_ueer.append(u)

    rows = zip(filename, temperature, frozen_total, f_ice, frozen_new_nr, frozen_new_ID, fice_leer, fice_ueer)
    with open(des+"/Output_"+os.path.basename(folderPath)+".csv", "w", newline='') as csv_file:
        writer = csv.writer(csv_file, delimiter=',')
        header = ['image_filename', 'temperature', 'frozen_total', 'f_ice', 'frozen_new', 'frozen_new_ID','fice_lerr', 'fice_uerr']
        writer.writerow(header)
        for row in rows:
            writer.writerow(row)

def CSVtoSqlite3(filename,des):
    global folderPath
    image_filename = filename
    con = sqlite3.connect("images2.db")
    cur = con.cursor()
    cur.execute("DROP TABLE IF EXISTS t;")
    cur.execute("CREATE TABLE IF NOT EXISTS t (image_filename,temperature,frozen_total,f_ice,frozen_new,frozen_new_ID);")

    with open(des+"/Output_"+os.path.basename(folderPath)+".csv", 'r') as fin:
        # csv.DictReader uses first line in file for column headings by default
        dr = csv.DictReader(fin)  # comma is default delimiter
        to_db = [(i['image_filename'], i['temperature'], i['frozen_total'], i['f_ice'],i['frozen_new'],i['frozen_new_ID']) for i in dr]

    cur.executemany("INSERT INTO t (image_filename,temperature,frozen_total,f_ice,frozen_new,frozen_new_ID) VALUES (?, ?, ?, ?, ?, ?);", to_db)
    con.commit()
    con.close()

#ergebnis in eine neue Spalte darstellen -> output daten
def poisson_error(fice, N_tot, alpha=1.96):
    """
    Gives the Poisson error for a given frozen fraction. After Agresti & Coull 1991.
    Returns lower and upper margins as "absolute" and "relative" fice value
    Parameters
    ----------
    fice : float
        frozen fraction.
    N_tot : integer
        Total number of droplets. INDA -> 96; LINA -> 90
    alpha : float, optional
        confidence interval(?) The default is 1.96.

    Returns
    -------
    l : float
        lower margin (as fice value)
    u : float
        upper margin (as fice value)
    fice - l: float
        length of the lower error bar
    u - fice: float
        length of the upper error bar

    """
    from numpy import sqrt as SQRT
    l = (fice + ((alpha ** 2) / (2 * N_tot)) - alpha * SQRT((fice * (1 - fice) + alpha ** 2 / (4 * N_tot)) / N_tot)) / (
                1 + (alpha ** 2 / N_tot))
    u = (fice + ((alpha ** 2) / (2 * N_tot)) + alpha * SQRT((fice * (1 - fice) + alpha ** 2 / (4 * N_tot)) / N_tot)) / (
                1 + (alpha ** 2 / N_tot))
    return (fice - l), (u - fice)

def printResult():
    print("Number of detected pictures:")
    print(len(results))
    print("-------------------------------------------------")
    print("image_filename:")
    print(image_filename)
    print("-------------------------------------------------")
    print("length of temperature:")
    print(len(temperature))
    print("Temperature from Tlog.csv File: ")
    print(temperature)
    print("-------------------------------------------------")
    print("length of frozen_total:")
    print(len(frozen_total))
    print("frozen_total:")
    print(frozen_total)
    print("--------------------------------------------------")
    print("length of f_ice")
    print(len(f_ice))
    print("f_ice:")
    print(f_ice)
    print("----------------------------------------------------")
    print("length of frozen_new")
    print(len(frozen_new_nr))
    print("frozen_new:")
    print(frozen_new_nr)
    print("-----------------------------------------------------")
    print("length of frozen_new_ID")
    print(len(frozen_new_ID))
    print("frozen_new_ID:")
    print(frozen_new_ID)

# Defining main function to be run
def main():

    loadVariables()
    # create destination folder
    desDir("/result_multi/")

    # analyse selected folder
    runDetection()

    # get Temperature
    getTemperature()

    # calculate frozen_total,f_ice, frozen_new, frozen_new_ID
    calculateVariables(results)

    # Print results
    printResult()

    # plot chart
    plotChart(des_dir)

    # export CSV
    exportCSV(image_filename,des_dir)

    # CSV to sqlite db3
    CSVtoSqlite3(image_filename,des_dir)

    # frozen cells stay frozen
    boundaryCondition.main()

    # run file oldProgram.py
    oldProgram.main()

    # draw bounding box
    drawBoundingBox.main()

    # open des_dir folder in window explorer
    os.startfile(des_dir)

# Checking if current namespace is main, that is file is not imported
if __name__ == '__main__':
    # Implementing main() function
    main()