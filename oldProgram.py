import collections
import json
import sys
import string
from PIL import Image, ImageDraw
from matplotlib.pyplot import cool
from progress.bar import IncrementalBar
import os
from matplotlib import pyplot
import csv
from numpy import array as array
import collections
import pandas as pd
import ast
import pickle
from pathlib import Path
from storage import storeDifference
from storage import storeOldProgramFrozenNewID


## Calibration Tool: https://codepen.io/backoefer/full/axbmyG
# Settings
total_droplet_number = 96.


brightness_probe_size = 29  # Needs to be uneven, so the circle's mid point is defined by a single pixel and corners are all equally large

# Calibration
delta_unfrozen_treshold = 0.1  # E.g. 0.2 = If total brightness difference of a probe is lower than 0.2 of the largest brightness difference, consider it unfrozen. This is particularly necessary when working with partial datasets
delta_significance_threshold = 0.8  # default=0.5; for MQ=0.8 The relative strength of a delta brightness change required to count as frozen. For example, with this setting set to 0.2, we will recognize the first delta in the top 80% of deltas as the freezing one

#Variables
fixPos = []
file_path = ""
newResult = []
filename = ""
log_file = ""


root = Path(".")

probe_coordinates = []
all_images = []
probe_validity = []
frozen_counts = []
newly_frozen_ids = []



### FUNCTION DEFINITIONS ###
def loadVariables():
    global fixPos
    global file_path
    global newResult

    inputFile = 'fixPos.data'
    inputPath = root / "Data" / inputFile
    fd = open(inputPath, 'rb')
    fixPos = pickle.load(fd)

    inputFile = 'folderPath.data'
    inputPath =root / "Data" / inputFile
    fd = open(inputPath, 'rb')
    file_path = pickle.load(fd)
    file_path += "/"

    inputFile = 'frozenNewID.data'
    inputPath = root / "Data" / inputFile
    fd = open(inputPath, 'rb')
    newResult = pickle.load(fd)

    print("load variables")
# Get Tlog.csv file in folder
def getTlogFile():
    global log_file
    global file_path
    global filename

    for r, d, f in os.walk(file_path):
        for file in f:
            # read csv file
            if file.endswith("Tlog.csv"):
                filename = file
    log_file = file_path + filename

# Function to get calibration file
def get_calibration():
    global probe_coordinates
    temp_coordinates = []
    input = fixPos
    for i in (range(len(input))):
        for j in (range(len(input[i]))):
            xmin = input[i][j][0]
            ymin = input[i][j][1]
            boxwidth = input[i][j][2]
            xmax = xmin + boxwidth
            boxheight = input[i][j][3]
            ymax = ymin + boxheight

            #using middle point formula
            xCenter = (xmin + xmax)/2
            xCenter = round(xCenter - 0.5) + 0.5

            yCenter = (ymin + ymax)/2
            yCenter = round(yCenter - 0.5) + 0.5

            row = input[i][j][6]
            column = input[i][j][7]

            id = ''
            id += row
            id += column
            tempDict = {'x': xCenter, 'y': yCenter, 'row': row, 'column': int(column), 'id': id}
            temp_coordinates.append(tempDict)

    #refine coordinates
    coordinates = []
    for i in range(8):
        for x in range(i, 96, 8):
            coordinates.append(temp_coordinates[x])
    probe_coordinates = coordinates
    return probe_coordinates

# Function to get all images in the image folder
def get_images():
    global all_images
    print('Open image files...')

    folder_contents = os.listdir(file_path)
    image_extensions = ('.jpg', '.jpeg')
    image_files = []
    for i, filename in enumerate(folder_contents):

        file_is_visible = filename[:1] != '.'
        file_exists = os.path.isfile(file_path + filename)
        file_is_image = any(ext in filename for ext in image_extensions)

        if file_exists and file_is_image and file_is_visible:
            # checking if image to be detected really belongs to selected folder?
            # avoiding the case wrong file with ending ".jpg" in selected folder
            split_fname = filename.split("_")
            if split_fname[1] in str(file_path):
                image_files.append(filename)
    # Needs to be sorted, as files aren't read in ascending order by default
    all_images = sorted(image_files)
    return all_images


# Create a circular mask that is used to constrain the sampled image area to a circle
def make_probe_mask(size):
    mask_image = Image.new('L', (size, size), 'black')  # Make black image
    drawing_context = ImageDraw.Draw(mask_image)
    drawing_context.ellipse([0, 0, size, size], 'white')  # Draw white circle on it
    return mask_image
brightness_probe_mask = make_probe_mask(brightness_probe_size)

# Use the histogram of a grayscale image to calculate the mean brightness between 0.0 (black) and 1.0 (white)
def calc_mean_brightness(histogram):
    pixel_count = float(sum(histogram))
    val = 0.0
    for i, n in enumerate(histogram):
        val = val + (float(n) * float(i)) / pixel_count
    normalized_val = val / 255.0  # Convert from 0-255 to 0.0-1.0
    return normalized_val


# Take a circular sample of pixels at the given coordinates
def probe_image_at_point(image, x, y):
    # Determine probe bounds
    half_size = float(brightness_probe_size) / 2.0
    probe_bounds = (x - half_size, y - half_size, x + half_size, y + half_size)

    # Crop probe sample
    cropped_image = image.crop(probe_bounds)
    # Create histogram with mask
    histogram = cropped_image.histogram(brightness_probe_mask)

    # Calculate mean gray
    mean_brightness = calc_mean_brightness(histogram)
    return mean_brightness


# Utility function for getting individual columns from two-dimensional arrays
def get_column(matrix, i):
    return [row[i] for row in matrix]


# Get temperatures from log file
def get_T():
    with open(log_file, 'r') as infile:
        csv_contents = csv.reader(infile, delimiter=';')
        T = get_column(csv_contents, 3)
        # Convert to floats
        for i, temp in enumerate(T):
            T[i] = float(temp)
        return T


def get_Tcalib():
    with open(log_file, 'r') as infile:
        csv_contents = csv.reader(infile, delimiter=';')
        Tcalib = get_column(csv_contents, 2)
        # Convert to floats
        for i, temp in enumerate(Tcalib):
            Tcalib[i] = float(temp)
        return Tcalib


# Save an overlay of the sample grid to verify sample locations
def save_sample_map(images):
    last_image = Image.open(file_path + images[len(images) - 1])  # Get last image as image object
    drawing_context = ImageDraw.Draw(last_image)

    # Half size
    half_size = float(brightness_probe_size) / 2.0

    # Draw red dots in sample locations
    for i, coords in enumerate(probe_coordinates):
        x = coords['x']
        y = coords['y']
        ellipse_bounds = [x - half_size, y - half_size, x + half_size, y + half_size]
        color = 'cyan'
        if probe_validity[i] == False:
            color = 'red'
        drawing_context.ellipse(ellipse_bounds, color)  # Draw red circle

    # Save test image
    last_image.save(file_path + 'sample_map_new.png', 'PNG')


# Take brightness samples for whole image set
def measure_brightness(images):
    rows = []
    bar = IncrementalBar('Measure brightness...', max=len(images), suffix='%(percent).1f%% - %(eta)ds')
    for i, image in enumerate(images):
        im = Image.open(file_path + image).convert(mode='L');
        cols = []
        for j, coords in enumerate(probe_coordinates):
            x = coords['x']
            y = coords['y']
            brightness = probe_image_at_point(im, x, y)
            cols.append(brightness)
        rows.append(cols)
        bar.next()

    bar.finish()
    return rows


# Reject unfrozen probes
def reject_unfrozen_probes(matrix):
    global probe_validity
    # Find total brightness delta for each probe
    brightness_deltas = [0.0 for x in range(96)]
    for i, probe in enumerate(matrix[0]):
        probes = get_column(matrix, i)
        min_brightness = min(probes)
        max_brightness = max(probes)
        brightness_deltas[i] = max_brightness - min_brightness

    # Get smalles/largest delta across all probes
    max_delta = max(brightness_deltas)

    # Reject probes that don't have sufficient brightness delta
    rejected_count = 0
    probe_validity = [True for x in range(96)]
    invalid_probes = []
    for i, delta in enumerate(brightness_deltas):
        delta_percentile = delta / max_delta
        if delta_percentile <= delta_unfrozen_treshold:
            invalid_probes.append(probe_coordinates[i]['id'])
            rejected_count = rejected_count + 1
            probe_validity[i] = False

    if (rejected_count > 0):
        # TODO: Mention which ones don't freeze
        print('WARNING: Rejected ' + str(rejected_count) + ' sample as unfrozen: ' + ' , '.join(
            invalid_probes))  # Show which probes are invalid

    return probe_validity


# Determine frozen sample count for all images
def analyze_data_incremental(matrix, images):
    global newly_frozen_ids
    global frozen_counts
    print('Analyzing data...')

    mins = [None for x in range(96)]
    maxs = [0.0 for x in range(96)]

    # Empty 2 dimensional matrix to store delta values for all probes in all images
    deltas = [[0.0 for probe in range(96)] for image in range(len(images))]

    # Determine delta changes for all measured brightnesses
    for r, row in enumerate(matrix):
        for c, col in enumerate(row):
            val = 0
            if r != 0:

                # Process delta brightness changes between images
                brightness_delta = matrix[r][c] - matrix[r - 1][c]  # Brightness delta for this probe, image over image
                brightness_delta = min(brightness_delta, 0)  # Ignore positive delta changes
                brightness_delta = abs(brightness_delta)  # Make delta asolute
                val = brightness_delta

                # Determine min and max deltas for this probe
                if val > maxs[c]:
                    maxs[c] = val
                if mins[c] == None or val < mins[c]:
                    mins[c] = val

            deltas[r][c] = val

    # print(deltas)
    # Determine frozen counts
    frozen_counts = []
    frozen_memory = [False for x in range(96)]
    # newly_frozen_ids = ['' for x in range(len(matrix))]
    newly_frozen_ids = [[] for x in range(len(matrix))]

    for i, row in enumerate(deltas):
        frozen_count = 0
        for j, sample in enumerate(row):

            # Skip probes that never freeze
            if probe_validity[j] == False:
                continue

            # Determine percentile of this delta change
            sample_percentile = (sample - mins[j]) / (maxs[j] - mins[j])

            newly_frozen = sample_percentile >= delta_significance_threshold and not frozen_memory[j]

            if newly_frozen:
                # if newly_frozen_ids[i] is not '':
                # newly_frozen_ids[i] += ', '
                # newly_frozen_ids[i] += probe_coordinates[j]['id']
                newly_frozen_ids[i].append(probe_coordinates[j]['id'])
            # If the delta change is higher than the defined threshold percentile, mark as frozen
            if newly_frozen or frozen_memory[j]:
                frozen_count = frozen_count + 1
                if not frozen_memory[j]:
                    frozen_memory[j] = True

        frozen_counts.append(frozen_count)
    return (frozen_counts, newly_frozen_ids)

#TODO
#ergebnis in eine neue Spalte darstellen -> output daten
def poisson_error(fice, N_tot, alpha=1.96):
    """
    Gives the Poisson error for a given frozen fraction. After Agresti & Coull 1991.
    Returns lower and upper margins as "absolute" and "relative" fice value
    Parameters
    ----------
    fice : float
        frozen fraction.
    N_tot : integer
        Total number of droplets. INDA -> 96; LINA -> 90
    alpha : float, optional
        confidence interval(?) The default is 1.96.

    Returns
    -------
    l : float
        lower margin (as fice value)
    u : float
        upper margin (as fice value)
    fice - l: float
        length of the lower error bar
    u - fice: float
        length of the upper error bar

    """
    from numpy import sqrt as SQRT
    l = (fice + ((alpha ** 2) / (2 * N_tot)) - alpha * SQRT((fice * (1 - fice) + alpha ** 2 / (4 * N_tot)) / N_tot)) / (
                1 + (alpha ** 2 / N_tot))
    u = (fice + ((alpha ** 2) / (2 * N_tot)) + alpha * SQRT((fice * (1 - fice) + alpha ** 2 / (4 * N_tot)) / N_tot)) / (
                1 + (alpha ** 2 / N_tot))
    return (fice - l), (u - fice)


# Plot results
def plot_results(temperatures, frozen_counts):
    print('Graphing results and saving as graph.png...')

    pyplot.xlim(round(min(temperatures)) - 2.0, 0.0)
    pyplot.ylabel('frozen fraction')
    pyplot.xlabel('Temperatur in C')
    fice = array([float(x) / total_droplet_number for x in frozen_counts])
    pyplot.errorbar(temperatures, fice, yerr=poisson_error(fice, total_droplet_number), c='#E24A33', ecolor='salmon',
                    elinewidth=0.2)

    pyplot.savefig(file_path + 'graph.png', dpi=150, bbox_inches='tight')


# Save Results to CSV file
def save_results(images, T, Tcalib, frozen_counts, newly_frozen_ids):
    global file_path
    print('Saving output.csv...')

    csv_headers = ['# filename', 'T', 'Tcalib', 'ffr', 'fice', 'Newly Frozen #', 'Newly Frozen IDs', 'fice_lerr',
                   'fice_uerr']
    csv_rows = [csv_headers]

    prev_frozen_count = 0
    for i, image_name in enumerate(images):
        newly_frozen = frozen_counts[i] - prev_frozen_count
        csv_rows.append([image_name,
                         T[i],
                         Tcalib[i],
                         frozen_counts[i],
                         frozen_counts[i] / total_droplet_number,
                         newly_frozen, newly_frozen_ids[i],
                         poisson_error(frozen_counts[i] / total_droplet_number, total_droplet_number)[0],
                         poisson_error(frozen_counts[i] / total_droplet_number, total_droplet_number)[1]]
                        )

        prev_frozen_count = frozen_counts[i]

    src_dir = os.getcwd()
    file_path = file_path[:-1]
    basename = file_path.split("/")
    des_dir = src_dir + "/result_multi/" + basename[-1] + "/"
    new_dst_file_name = des_dir + 'output_OLD_PROGRAM.csv'
    with open(new_dst_file_name, 'w') as outfile:
        writer = csv.writer(outfile, lineterminator='\n')
        writer.writerows(csv_rows)
        outfile.close()


def compareToOldProgram(input):
    new_result = newResult
    old_result = input

    print("NEW PROGRAM - NEW FROZEN ID:")
    print(new_result)

    print("OLD PROGRAM - NEW FROZEN ID:")
    print(old_result)

    percentage = 0
    difference = []
    for i in (range(len(new_result))):
        #comparing two list without caring about order of list item
        if collections.Counter(new_result[i]) == collections.Counter(old_result[i]):
            percentage += 1
            difference.append([])
        else:
            # The symmetric difference of two sets new_result and old_result is the set of elements that are in either new_result or old_result
            diff = set(new_result[i]).symmetric_difference(set(old_result[i]))
            diff = list(diff)
            difference.append(diff)
    print("######################################################################################################")
    print("Difference - new and old Program:")
    print(difference)
    #save difference
    storeDifference(difference)

    percentage = percentage / len(new_result) * 100
    print("Similarity of frozen_new_ID between two PROGRAMS:")
    print(str(percentage) + "%")


# Defining main function to be run
def main():

    # ### SCRIPT ###

    # # -1. Load variables
    loadVariables()

    # # -1.2.Get Tlog.csv file in folder
    getTlogFile()
    # # 0. Get Calibration
    get_calibration()


    # # 1. Get image files + log file with temperatures
    all_images = get_images()
    # print(all_images)

    Tcalib = get_Tcalib()

    T = get_T()

    # # 2. Measure brightness for all samples across all images
    brightness_data = measure_brightness(all_images)
    # print(brightness_data)

    reject_unfrozen_probes(brightness_data)  # Check for permanently unfrozen probes

    save_sample_map(all_images)

    # # 3. Determine when each sample froze
    frozen_sample_counts, newly_frozen_ids = analyze_data_incremental(brightness_data, all_images)


    # # 4. Plot results
    plot_results(Tcalib, frozen_sample_counts)

    # # 5. Saving results

    save_results(all_images, T, Tcalib, frozen_sample_counts, newly_frozen_ids)

    #tore newly_frozen_ids from old program
    storeOldProgramFrozenNewID(newly_frozen_ids)
    # # 6. Compare 2 programs
    compareToOldProgram(newly_frozen_ids)




# Checking if current namespace is main, that is file is not imported
if __name__ == '__main__':
    # Implementing main() function
    main()
