import shutil
import sys, os
import threading
import time

from distutils.dir_util import copy_tree
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
import sqlite3
import textwrap

import drawBoxUpdate
import updateData
import pickle
from pathlib import Path
from storage import storeResultsWarning

con = sqlite3.connect("images2.db")
cur = con.cursor()

fixPos = []
folderPath = ""
results = []
resultsWarning = []
resultsOriginal = []
root = Path(".")
difference = []
textInfo = ""
textInfo1 = ""
textInfo2 = ""
textInfo3 = ""

frozenNewID = []
oldProgramFrozenNewID = []


def loadVariables():
    global fixPos
    global folderPath
    global results
    global resultsWarning
    global resultsOriginal
    global difference
    global frozenNewID
    global oldProgramFrozenNewID

    # load fixPos
    inputFile = 'fixPos.data'
    inputPath = root / "Data" / inputFile
    fd = open(inputPath, 'rb')
    fixPos = pickle.load(fd)

    # load folderPath
    inputFile = 'folderPath.data'
    inputPath = root / "Data" / inputFile
    fd = open(inputPath, 'rb')
    folderPath = pickle.load(fd)

    # load results
    inputFile = 'results.data'
    inputPath = root / "Data" / inputFile
    fd = open(inputPath, 'rb')
    results = pickle.load(fd)

    # load warningBox
    inputFile = 'resultsWarning.data'
    inputPath = root / "Data" / inputFile
    fd = open(inputPath, 'rb')
    resultsWarning = pickle.load(fd)

    # load resultsOriginal
    inputFile = 'resultsOriginal.data'
    inputPath = root / "Data" / inputFile
    fd = open(inputPath, 'rb')
    resultsOriginal = pickle.load(fd)

    # load difference
    inputFile = 'difference.data'
    inputPath = root / "Data" / inputFile
    fd = open(inputPath, 'rb')
    difference = pickle.load(fd)

    # load newFrozenID from new program
    inputFile = 'frozenNewID.data'
    inputPath = root / "Data" / inputFile
    fd = open(inputPath, 'rb')
    frozenNewID = pickle.load(fd)

    # load newFrozenID from old program
    inputFile = 'oldProgramFrozenNewID.data'
    inputPath = root / "Data" / inputFile
    fd = open(inputPath, 'rb')
    oldProgramFrozenNewID = pickle.load(fd)


def copyResults():
    # save detected image to destination folder
    current_dir = os.getcwd()
    des_dir = current_dir + "/result_multi_update/" + os.path.basename(folderPath)
    src_dir = current_dir + "/result_multi/" + os.path.basename(folderPath)
    # create folder
    if not os.path.exists(des_dir):
        copy_tree(src_dir, des_dir)


class Main(QMainWindow):
    global textInfo
    global textInfo1
    global textInfo2
    global textInfo3

    def __init__(self):
        super().__init__()
        self.setWindowTitle("INDA Detection")
        self.setWindowIcon(QIcon('icons/icon3.ico'))
        self.setGeometry(50, 70, 1920, 1080)
        self.showMaximized()
        self.UI()
        self.show()

    def UI(self):
        self.toolBar()
        self.tabWigdet()
        self.widgets()
        self.layouts()
        self.displayImages()

    # 3 buttons on top
    def toolBar(self):
        self.tb = self.addToolBar("Tool Bar")
        self.tb.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        #####################Toolbar Buttons############
        ####################Open single Data ################
        self.addImage = QAction(QIcon('icons/add.png'), "Open single Data", self)
        self.tb.addAction(self.addImage)
        self.addImage.triggered.connect(self.openSingleData)
        self.tb.addSeparator()
        ######################Open multiple Data################
        self.addImages = QAction(QIcon('icons/add.png'), "Open multile Data", self)
        self.tb.addAction(self.addImages)
        self.addImages.triggered.connect(self.openMultipleData)
        self.tb.addSeparator()

    # tab widget
    def tabWigdet(self):
        self.tabs = QTabWidget()
        self.tabs.blockSignals(True)
        self.tabs.currentChanged.connect(self.tabChanged)
        self.setCentralWidget(self.tabs)
        self.tab1 = QWidget()
        self.tab2 = QWidget()
        self.tab3 = QWidget()
        self.tabs.addTab(self.tab1, "Normal Detection")
        self.tabs.addTab(self.tab2, "Detection < 96")
        self.tabs.addTab(self.tab3, "Detection Left/Right")

    def widgets(self):

        #######################Tab1 Widgets###############
        ####################Main left layout widget##########
        self.itemsTable = QTableWidget()
        self.itemsTable.setColumnCount(6)

        self.itemsTable.setHorizontalHeaderItem(0, QTableWidgetItem("image_filename"))
        self.itemsTable.setHorizontalHeaderItem(1, QTableWidgetItem("temperature"))
        self.itemsTable.setHorizontalHeaderItem(2, QTableWidgetItem("frozen_total"))
        self.itemsTable.setHorizontalHeaderItem(3, QTableWidgetItem("f_ice"))
        self.itemsTable.setHorizontalHeaderItem(4, QTableWidgetItem("frozen_new"))
        self.itemsTable.setHorizontalHeaderItem(5, QTableWidgetItem("frozen_new_ID"))

        self.itemsTable.horizontalHeader().setSectionResizeMode(0, QHeaderView.ResizeToContents)
        self.itemsTable.horizontalHeader().setSectionResizeMode(1, QHeaderView.Stretch)
        self.itemsTable.horizontalHeader().setSectionResizeMode(2, QHeaderView.Stretch)
        self.itemsTable.horizontalHeader().setSectionResizeMode(3, QHeaderView.Stretch)
        self.itemsTable.horizontalHeader().setSectionResizeMode(4, QHeaderView.Stretch)
        self.itemsTable.horizontalHeader().setSectionResizeMode(5, QHeaderView.ResizeToContents)

        self.itemsTable.doubleClicked.connect(self.selectedItem)

        ########################Right top layout widgets#######################
        self.searchText = QLabel("Search")
        self.searchEntry = QLineEdit()
        self.searchEntry.setPlaceholderText("Search For Data")
        self.searchButton = QPushButton("Search")
        self.searchButton.clicked.connect(self.searchItems)

        ##########################Right middle layout widgets###########
        self.allItems = QRadioButton("All")
        self.onlyNewFrozenItems = QRadioButton("Only New Frozen")
        self.listButton = QPushButton("List")
        self.listButton.clicked.connect(self.listItems)

        ##########################Right bottom layout widgets###########
        self.BrowserDetectedDataButton = QPushButton("Open Detected Folder")
        self.BrowserDetectedDataButton.clicked.connect(self.openDetectedFolder)

        ##########################Right bottom2 layout widgets###########

        self.textInfo = QLabel(textInfo)
        self.textInfo.setFont(QFont("Arial", 8))
        self.textInfo.setStyleSheet("color: black;")
        self.textInfo.setAlignment(Qt.AlignTop)

        self.textInfo1 = QLabel(textInfo1)
        self.textInfo1.setFont(QFont("Arial", 8))
        self.textInfo1.setStyleSheet("color: black;")
        self.textInfo1.setAlignment(Qt.AlignTop)

        self.textInfo2 = QLabel(textInfo2)
        self.textInfo2.setFont(QFont("Arial", 8))
        self.textInfo2.setStyleSheet("color: black;")
        self.textInfo2.setAlignment(Qt.AlignTop)

        self.textInfo3 = QLabel(textInfo3)
        self.textInfo3.setFont(QFont("Arial", 15))
        self.textInfo3.setStyleSheet("color: black;")
        self.textInfo3.setAlignment(Qt.AlignTop)

    def layouts(self):
        ######################Tab1 layouts##############
        self.mainLayout = QHBoxLayout()
        self.mainLeftLayout = QVBoxLayout()
        self.mainRightLayout = QVBoxLayout()
        self.rightTopLayout = QHBoxLayout()
        self.rightMiddleLayout = QHBoxLayout()
        self.rightBottomLayout = QHBoxLayout()
        self.rightBottomLayout2 = QVBoxLayout()

        self.topGroupBox = QGroupBox("Search Detected Data")
        self.middleGroupBox = QGroupBox("List Detected Data")
        self.bottomGroupBox = QGroupBox()
        self.bottomGroupBox2 = QGroupBox("Information Log")

        #################Add widgets###################
        ################Left main layout widget###########
        self.mainLeftLayout.addWidget(self.itemsTable)

        ########################Right top layout widgets#########
        self.rightTopLayout.addWidget(self.searchText)
        self.rightTopLayout.addWidget(self.searchEntry)
        self.rightTopLayout.addWidget(self.searchButton)
        self.topGroupBox.setLayout(self.rightTopLayout)

        #################Right middle layout widgets##########
        self.rightMiddleLayout.addWidget(self.allItems)
        self.rightMiddleLayout.addWidget(self.onlyNewFrozenItems)
        self.rightMiddleLayout.addWidget(self.listButton)
        self.middleGroupBox.setLayout(self.rightMiddleLayout)

        #################Right bottom layout widgets##########
        self.rightBottomLayout.addWidget(self.BrowserDetectedDataButton)
        self.rightBottomLayout.addStretch()
        self.bottomGroupBox.setLayout(self.rightBottomLayout)

        #################Right bottom2 layout widgets##########
        self.rightBottomLayout2.addWidget(self.textInfo)
        #self.rightBottomLayout2.addWidget(self.textInfo1)
        #self.rightBottomLayout2.addWidget(self.textInfo2)
        self.rightBottomLayout2.addWidget(self.textInfo3)

        self.rightTopLayout.addStretch()
        self.bottomGroupBox2.setLayout(self.rightBottomLayout2)

        self.mainRightLayout.addWidget(self.topGroupBox, 20)
        self.mainRightLayout.addWidget(self.middleGroupBox, 20)
        self.mainRightLayout.addWidget(self.bottomGroupBox, 10)
        self.mainRightLayout.addWidget(self.bottomGroupBox2, 50)
        self.mainLayout.addLayout(self.mainLeftLayout, 70)
        self.mainLayout.addLayout(self.mainRightLayout, 30)
        self.tab1.setLayout(self.mainLayout)

    def openSingleData(self):
        self.showMinimized()
        os.system('python gui-yolo-3.py')

    def openMultipleData(self):
        self.showMinimized()
        os.system('python gui-yolo-3-multi.py')

    def displayImages(self):
        self.itemsTable.setFont(QFont("Times", 12))
        for i in reversed(range(self.itemsTable.rowCount())):
            self.itemsTable.removeRow(i)

        query = cur.execute("SELECT image_filename,temperature,frozen_total,f_ice,frozen_new,frozen_new_ID FROM t")
        for row_data in query:
            row_number = self.itemsTable.rowCount()
            self.itemsTable.insertRow(row_number)
            for column_number, data in enumerate(row_data):
                self.itemsTable.setItem(row_number, column_number, QTableWidgetItem(str(data)))

        self.itemsTable.setEditTriggers(QAbstractItemView.NoEditTriggers)

    def searchItems(self):
        value = self.searchEntry.text()
        if value == "":
            QMessageBox.information(self, "Warning", "Search query cant be empty!!!")

        else:
            self.searchEntry.setText("")

            query = (
                "SELECT image_filename,temperature,frozen_total,f_ice,frozen_new,frozen_new_ID FROM t WHERE image_filename LIKE ? or frozen_new_ID LIKE ?")
            results = cur.execute(query, ('%' + value + '%', '%' + value + '%')).fetchall()
            print(results)

            if results == []:
                QMessageBox.information(self, "Warning", "There is no such a item ")

            else:
                for i in reversed(range(self.itemsTable.rowCount())):
                    self.itemsTable.removeRow(i)

                for row_data in results:
                    row_number = self.itemsTable.rowCount()
                    self.itemsTable.insertRow(row_number)
                    for column_number, data in enumerate(row_data):
                        self.itemsTable.setItem(row_number, column_number, QTableWidgetItem(str(data)))

    def listItems(self):
        if self.allItems.isChecked() == True:
            self.displayImages()
        elif self.onlyNewFrozenItems.isChecked():
            query = (
                "SELECT image_filename,temperature,frozen_total,f_ice,frozen_new,frozen_new_ID FROM t WHERE frozen_new_ID <>'[]'")
            items = cur.execute(query).fetchall()
            print(items)

            for i in reversed(range(self.itemsTable.rowCount())):
                self.itemsTable.removeRow(i)

            for row_data in items:
                row_number = self.itemsTable.rowCount()
                self.itemsTable.insertRow(row_number)
                for column_number, data in enumerate(row_data):
                    self.itemsTable.setItem(row_number, column_number, QTableWidgetItem(str(data)))

    def openDetectedFolder(self):
        global des_dir
        global folderPath

        src_dir = os.getcwd()
        des_dir = src_dir + "/result_multi/" + os.path.basename(folderPath)
        os.startfile(des_dir)

    def tabChanged(self):
        self.displayImages()

    ################################################################################################
    def selectedItem(self):
        global imageID
        listItem = []
        for i in range(0, 6):
            listItem.append(self.itemsTable.item(self.itemsTable.currentRow(), i).text())

        imageID = listItem[0]
        print(imageID)
        self.display = DisplayImage()
        self.display.show()


class DisplayImage(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Data Details")
        self.setWindowIcon(QIcon('icons/icon3.ico'))
        self.setGeometry(0, 0, 1920, 1080)
        self.showMaximized()
        # self.setFixedSize(1920,1000)
        self.UI()
        self.show()

    def UI(self):
        self.imageDetails()
        self.mainDesign()
        self.layouts()
        self.getImages()
        self.displayFirstRecord()

    def mainDesign(self):
        self.item_Img = QLabel()
        self.item_Img.setFixedSize(1280, 960)

        self.imageList = QListWidget()
        self.imageList.itemSelectionChanged.connect(self.singleCLickOrButtonUpAndDown)

        self.comboColumn = QComboBox(self)
        self.comboRow = QComboBox(self)
        self.setText = QLabel("Set new frozen at ID:")
        self.btnSet = QPushButton("Set")
        self.btnSet.clicked.connect(self.getIDValue)
        self.comboColumn.addItems(["A", "B", "C", "D", "E", "F", "G", "H"])
        self.comboRow.addItems(["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"])

        self.btnUpdate = QPushButton("Update")
        self.btnUpdate.clicked.connect(self.updateImage)
        self.btnReset = QPushButton("Reset")
        self.btnReset.clicked.connect(self.resetImage)
        self.text1 = QLabel("Yellow: liquid")
        self.text1.setFont(QFont("Arial", 12))
        self.text1.setStyleSheet("color: yellow")
        self.text2 = QLabel("Blue: frozen")
        self.text2.setFont(QFont("Arial", 12))
        self.text2.setStyleSheet("color: blue;")
        self.text3 = QLabel("Red: warning (difference to old program)")
        self.text3.setFont(QFont("Arial", 12))
        self.text3.setStyleSheet("color: red;")
        self.blank = QLabel("")
        self.text4 = QLabel("Use left mouse or UP/DOWN key to select item ")
        self.text4.setFont(QFont("Arial", 12))
        self.text4.setStyleSheet("color: black;")

    def layouts(self):
        ###################Layouts###############
        self.mainLayout = QHBoxLayout()
        self.leftMainLayout = QHBoxLayout()
        self.leftLeftLayout = QHBoxLayout()
        self.leftRightLayout = QFormLayout()
        self.rightMainLayout = QVBoxLayout()
        self.rightTopLayout = QHBoxLayout()

        self.hbox = QHBoxLayout()
        self.hboxSelect = QHBoxLayout()
        self.vbox = QVBoxLayout()

        #####################Adding child layouts to main layout###########
        self.rightMainLayout.addLayout(self.rightTopLayout)
        self.leftMainLayout.addLayout(self.leftLeftLayout)
        self.leftMainLayout.addLayout(self.leftRightLayout)
        self.mainLayout.addLayout(self.leftMainLayout, 90)
        self.mainLayout.addLayout(self.rightMainLayout, 10)
        ###################adding wigdets to layouts#################
        self.rightTopLayout.addWidget(self.imageList)
        self.rightTopLayout.addStretch()

        self.hboxSelect.addStretch()
        self.hboxSelect.addWidget(self.setText)
        self.hboxSelect.addWidget(self.comboColumn)
        self.hboxSelect.addWidget(self.comboRow)
        self.hboxSelect.addWidget(self.btnSet)
        self.hboxSelect.addStretch()

        self.hbox.addStretch()
        self.hbox.addWidget(self.btnUpdate)
        self.hbox.addWidget(self.btnReset)
        self.hbox.addStretch()

        self.vbox.addWidget(self.blank)
        self.vbox.addWidget(self.blank)
        self.vbox.addWidget(self.text1)
        self.vbox.addWidget(self.text2)
        self.vbox.addWidget(self.text3)
        self.vbox.addWidget(self.blank)
        self.vbox.addWidget(self.text4)
        self.vbox.addStretch()

        self.leftLeftLayout.addWidget(self.item_Img)
        self.leftLeftLayout.addStretch()
        ##############setting main window layout#####################
        self.setLayout(self.mainLayout)

    def getImages(self):
        query = "SELECT image_filename FROM t"
        images = cur.execute(query).fetchall()
        for image in images:
            image_str = str(image)
            image_str = image_str[2:]
            image_str = image_str[:-3]
            self.imageList.addItem(image_str)
        # Set current Row
        row = imageID.split("_")
        row_str = row[-1]
        row_str = row_str[:-4]
        row_int = int(row_str)
        self.imageList.setCurrentRow(row_int)

    def imageDetails(self):
        global imageID
        query = ("SELECT * FROM t WHERE image_filename = ?")
        items = cur.execute(query, (imageID,)).fetchone()
        self.image_filename = items[0]
        self.temperature = items[1]
        self.frozen_total = items[2]
        self.f_ice = items[3]
        self.frozen_new = items[4]
        self.frozen_new_ID = items[5]

    def displayFirstRecord(self):
        # Display Image
        src_dir = os.getcwd()
        des_dir = src_dir + "/result_multi/" + os.path.basename(folderPath)
        self.img = QPixmap(des_dir + '/' + self.image_filename)
        self.item_Img.setPixmap(self.img)
        # get mouse pos
        self.item_Img.mousePressEvent = self.getPos

    def singleCLickOrButtonUpAndDown(self):
        loadVariables()
        for i in reversed(range(self.leftRightLayout.count())):
            widget = self.leftRightLayout.takeAt(i).widget()

            if widget is not None:
                widget.deleteLater()

        item = self.imageList.currentItem().text()

        query = ("SELECT * FROM t WHERE image_filename = ?")
        items = cur.execute(query, (item,)).fetchone()
        self.image_filename = items[0]
        self.temperature = items[1]
        self.frozen_total = items[2]
        self.f_ice = items[3]
        self.frozen_new = items[4]
        self.frozen_new_ID = items[5]

        # Display Image
        src_dir = os.getcwd()
        des_dir = src_dir + "/result_multi/" + os.path.basename(folderPath)
        self.img = QPixmap(des_dir + '/' + self.image_filename)
        self.item_Img.setPixmap(self.img)

        # Display Infos
        temperature = QLabel(self.temperature)
        frozen_total = QLabel(self.frozen_total)
        f_ice = QLabel(self.f_ice)
        frozen_new = QLabel(self.frozen_new)

        frozen_new_ID_str = textwrap.fill(self.frozen_new_ID, 20)
        frozen_new_ID = QLabel(frozen_new_ID_str)

        self.leftRightLayout.setVerticalSpacing(20)
        self.leftRightLayout.addRow("temperature: ", temperature)
        self.leftRightLayout.addRow("frozen_total:", frozen_total)
        self.leftRightLayout.addRow("f_ice:", f_ice)
        self.leftRightLayout.addRow("frozen_new:", frozen_new)
        self.leftRightLayout.addRow("frozen_new_ID:", frozen_new_ID)
        self.leftRightLayout.addRow(self.hboxSelect)
        self.leftRightLayout.addRow(self.hbox)
        self.leftRightLayout.addRow(self.vbox)

    def getPos(self, event):
        x = event.pos().x()
        y = event.pos().y()
        pos = (x, y)
        self.decodePos(x, y, fixPos)

    # from mouse pos -> cell ID
    def decodePos(self, x, y, fixPosition):
        # print(fixPosition)
        # go through all cell in fixpos
        for i in range(len(fixPosition)):
            for j in range(len(fixPosition[i])):
                xmin = fixPosition[i][j][0]
                ymin = fixPosition[i][j][1]
                width = fixPosition[i][j][2]
                height = fixPosition[i][j][3]
                rect = (xmin, ymin, width, height)
                if self.rectangleContains(rect, x, y):
                    result = fixPosition[i][j][8]

                    print("Mouse is clicked at " + str(result))
                    # print(self.image_filename)
                    self.updateResults(result, self.image_filename)

    # check if point is contained within a bounding rectangle
    def rectangleContains(self, rect, x, y):
        result = rect[0] <= x < rect[0] + rect[2] and rect[1] <= y < rect[1] + rect[3]
        return result

    def updateResults(self, id, image_filename):
        global resultsWarning
        # get image number nr
        image_filename_split = image_filename.split("_")
        nr = image_filename_split[-1]
        nr = nr[:-4]
        nr = int(nr)

        # change results value
        for i in range(len(resultsWarning[nr])):
            for j in range(len(resultsWarning[nr][i])):
                if resultsWarning[nr][i][j][8] == id:
                    # 0 -> 1
                    if resultsWarning[nr][i][j][4] == 0:
                        result_list = list(resultsWarning[nr][i][j])
                        result_list[4] = 1
                        resultsWarning[nr][i][j] = tuple(result_list)
                        storeResultsWarning(resultsWarning)

                    # 1 -> 2
                    elif resultsWarning[nr][i][j][4] == 1:
                        result_list = list(resultsWarning[nr][i][j])
                        result_list[4] = 2
                        resultsWarning[nr][i][j] = tuple(result_list)
                        storeResultsWarning(resultsWarning)
                    # 2 -> 0
                    elif resultsWarning[nr][i][j][4] == 2:
                        result_list = list(resultsWarning[nr][i][j])
                        result_list[4] = 0
                        resultsWarning[nr][i][j] = tuple(result_list)
                        storeResultsWarning(resultsWarning)

        drawBoxUpdate.loadVariables()
        drawBoxUpdate.drawWarningBox(nr)

        # Display Image
        current_dir = os.getcwd()
        src_dir = current_dir + "/result_multi_update_temp/" + os.path.basename(folderPath)
        des_dir = current_dir + "/result_multi/" + os.path.basename(folderPath)

        self.img = QPixmap(os.path.join(src_dir, self.image_filename))
        self.item_Img.setPixmap(self.img)
        shutil.move(os.path.join(src_dir, self.image_filename), os.path.join(des_dir, self.image_filename))


    def getIDValue(self):
        global IDvalue
        IDvalue = self.comboColumn.currentText()
        IDvalue += self.comboRow.currentText()

        self.updateResultsViaSelection(IDvalue, self.image_filename)
        print(IDvalue)

    def updateResultsViaSelection(self, id, image_filename):
        global resultsWarning

        self.popUp = MyWarnings()
        self.popUp.show()
        self.helper = Helper()
        self.helper.finished.connect(self.popUp.close)


        # get image number nr
        image_filename_split = image_filename.split("_")
        nr = image_filename_split[-1]
        nr = nr[:-4]
        nr = int(nr)

        # change curernt results value
        for i in range(len(resultsWarning[nr])):
            for j in range(len(resultsWarning[nr][i])):
                if resultsWarning[nr][i][j][8] == id:
                    # 0 -> 1
                    if resultsWarning[nr][i][j][4] == 0:
                        result_list = list(resultsWarning[nr][i][j])
                        result_list[4] = 1
                        resultsWarning[nr][i][j] = tuple(result_list)
                        storeResultsWarning(resultsWarning)

                    # 2 -> 1
                    elif resultsWarning[nr][i][j][4] == 2:
                        result_list = list(resultsWarning[nr][i][j])
                        result_list[4] = 1
                        resultsWarning[nr][i][j] = tuple(result_list)
                        storeResultsWarning(resultsWarning)

        # automatic update previous cells according to new frozen ID
        for t in range(0, nr):
            for i in range(len(resultsWarning[t])):
                for j in range(len(resultsWarning[t][i])):
                    if resultsWarning[t][i][j][8] == id:
                        # 2 -> 0
                        if resultsWarning[t][i][j][4] == 2:
                            result_list = list(resultsWarning[t][i][j])
                            result_list[4] = 0
                            resultsWarning[t][i][j] = tuple(result_list)
                            storeResultsWarning(resultsWarning)

                        # 1 -> 0
                        elif resultsWarning[t][i][j][4] == 1:
                            result_list = list(resultsWarning[t][i][j])
                            result_list[4] = 0
                            resultsWarning[t][i][j] = tuple(result_list)
                            storeResultsWarning(resultsWarning)

        # automatic update afterward cells according to new frozen ID
        for t in range(nr + 1, len(resultsWarning)):
            for i in range(len(resultsWarning[t])):
                for j in range(len(resultsWarning[t][i])):
                    if resultsWarning[t][i][j][8] == id:
                        # 2 -> 1
                        if resultsWarning[t][i][j][4] == 2:
                            result_list = list(resultsWarning[t][i][j])
                            result_list[4] = 1
                            resultsWarning[t][i][j] = tuple(result_list)
                            storeResultsWarning(resultsWarning)

                        # 0 -> 1
                        elif resultsWarning[t][i][j][4] == 0:
                            result_list = list(resultsWarning[t][i][j])
                            result_list[4] = 1
                            resultsWarning[t][i][j] = tuple(result_list)
                            storeResultsWarning(resultsWarning)

        threading.Thread(target=reset, args=(self.helper,)).start()

    # click button Update
    def updateImage(self):
        updateData.main()

    # click button Update
    def resetImage(self):
        global resultsOriginal
        storeResultsWarning(resultsOriginal)
        self.popUp = MyWarnings()
        self.popUp.show()
        self.helper = Helper()
        self.helper.finished.connect(self.popUp.close)
        threading.Thread(target=reset, args=(self.helper,)).start()

def reset(helper):
    updateData.reset()
    helper.finished.emit()


class Helper(QObject):
    finished = pyqtSignal()


class MyWarnings(QDialog):
    def __init__(self):
        super(MyWarnings, self).__init__()
        self.setWindowTitle("Processing...")
        self.setWindowIcon(QIcon('icons/icon3.ico'))

        qtRectangle = self.frameGeometry()
        centerPoint = QDesktopWidget().availableGeometry().center()
        qtRectangle.moveCenter(centerPoint)
        self.move(qtRectangle.topLeft())

        widget1 = QLabel('Saving images...Please wait until this window is closed!')
        widget1.setFont(QFont('Arial', 15))
        layout = QVBoxLayout()
        layout.addWidget(widget1)
        self.setLayout(layout)


def getDifferenceText():
    global difference
    global frozenNewID
    global oldProgramFrozenNewID

    frozenNewIDStr = "New Frozen ID - New Program:    "
    oldProgramFrozenNewIDStr = "New FrozenID - Old Program:    "
    differenceStr = "Warning boxes at [image number : number of warning boxes] = "

    for i in range(len(frozenNewID)):
        if frozenNewID[i]:
            frozenNewIDStr += "(" + str(i) + "=" + str(frozenNewID[i]) + ") "

    for i in range(len(oldProgramFrozenNewID)):
        if oldProgramFrozenNewID[i]:
            oldProgramFrozenNewIDStr += "(" + str(i) + "=" + str(oldProgramFrozenNewID[i]) + ") "
    sumImage = 0
    sumWarningBoxes = 0
    for i in range(len(difference)):
        if difference[i]:
            #differenceStr += "(" + str(i) + "=" + str(difference[i]) + ") "
            differenceStr += "["+str(i)+" : "+str(len(difference[i]))+"] "
            sumWarningBoxes += len(difference[i])
            sumImage +=1
    print("Sum Image:")
    print(sumImage)
    print("Sum WarningBoxes:")
    print(sumWarningBoxes)
    frozenNewIDStr = textwrap.fill(str(frozenNewIDStr), 100)
    oldProgramFrozenNewIDStr = textwrap.fill(str(oldProgramFrozenNewIDStr), 100)

    differenceStr += ". SUM of images has warning: " +str(sumImage) + ". SUM of total red warning boxes: " + str(sumWarningBoxes)
    differenceStr = textwrap.fill(str(differenceStr), 50)
    return frozenNewIDStr, oldProgramFrozenNewIDStr, differenceStr


def main():
    global textInfo
    global textInfo1
    global textInfo2
    global textInfo3
    try:
        loadVariables()
        textInfo = "Information below is displayed for the purpose of comparing 2 programs :"
        textInfo1, textInfo2, textInfo3 = getDifferenceText()


    except Exception as e:
        textInfo = "No result found. Please select data for detection."
        textInfo = textwrap.fill(textInfo, 80)

    App = QApplication(sys.argv)
    window = Main()
    sys.exit(App.exec_())


if __name__ == '__main__':
    main()
