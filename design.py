

# Form implementation generated from reading ui file 'design.ui'



from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1919, 1000)
        MainWindow.setMaximumSize(QtCore.QSize(1920, 1080))
        self.showMaximized()
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.imgLabel = QtWidgets.QLabel(self.centralwidget)
        self.imgLabel.setMaximumSize(QtCore.QSize(940, 800))
        font = QtGui.QFont()
        font.setPointSize(7)
        font.setBold(False)
        font.setWeight(50)
        self.imgLabel.setFont(font)
        self.imgLabel.setText("")
        self.imgLabel.setObjectName("imgLabel")
        self.horizontalLayout.addWidget(self.imgLabel)
        self.processedLabel = QtWidgets.QLabel(self.centralwidget)
        self.processedLabel.setMaximumSize(QtCore.QSize(940, 800))
        self.processedLabel.setText("")
        self.processedLabel.setObjectName("processedLabel")
        self.horizontalLayout.addWidget(self.processedLabel)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.loadButton = QtWidgets.QPushButton(self.centralwidget)
        self.loadButton.setMaximumSize(QtCore.QSize(200, 16777215))
        self.loadButton.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.loadButton.setObjectName("loadButton")
        self.verticalLayout.addWidget(self.loadButton)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "YOLO"))
        self.loadButton.setText(_translate("MainWindow", "Load Image"))
