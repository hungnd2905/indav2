import os


from PyQt5 import QtWidgets
import design
from PyQt5 import QtCore
import sys
from storage import storeFolderPath
import detection

#initialize global variables
# folder path to be detected
folderPath = ""

# Creating GUI interface to load and detect multiple images
class MainApp(QtWidgets.QMainWindow, design.Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.image = None
        self.processedImage = None
        self.loadButton.clicked.connect(self.loadClicked)

        #folder path to be detected
        self.folderPath = ""

    def loadClicked(self):
        global folderPath

        self.folderPath = str(QtWidgets.QFileDialog.getExistingDirectory(self, "Select Directory"))
        print("--------")
        print("Path of input folder:")
        print(self.folderPath)
        print("-------")
        print("Name of folder:")
        print(os.path.basename(self.folderPath))
        print("---------")

        folderPath = self.folderPath
        storeFolderPath(folderPath)
        self.showMinimized()

        #if no folder is selected, quit app
        if self.folderPath =="":
            print("Folder is not selected")
            QtWidgets.qApp.quit()
            return False

        # Running detection
        detection.main()

        self.close()

    def keyPressEvent(self, e):
        if e.key() == QtCore.Qt.Key_Escape:
            self.close()
        if e.key() == QtCore.Qt.Key_F11:
            if self.isMaximized():
                self.showNormal()
            else:
                self.showMaximized()



# Defining main function to be run
def main():

    # Initializing instance of Qt Application
    app = QtWidgets.QApplication(sys.argv)

    # Initializing object of designed GUI
    window = MainApp()

    # Showing designed GUI
    window.show()
    try:
        # Running application
        app.exec_()
    except Exception as e:
        print("Error occurred!")

# Checking if current namespace is main, that is file is not imported
if __name__ == '__main__':
    # Implementing main() function
    main()
